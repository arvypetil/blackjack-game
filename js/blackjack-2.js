// Card object.
class Card {
  constructor(suit, value) {
    this.suit = suit;
    this.value = value;
  }
}

// Deck object.
class Deck {
  constructor() {
    this.suits = ['Spades', 'Hearts', 'Clubs', 'Diamonds'];
    this.values = ['A', '2', '3', '4', '5', '6', '7', '8', '9', '10',
      'J', 'Q', 'K'];

    this.cards = [];

    this.suits.forEach((suit) => {
      this.values.forEach((value) => {
        this.cards.push(new Card(suit, value));
      });
    });
  }

  shuffle() {
    // Fisher-yates shuffle
    for (let currentIndex = this.cards.length - 1; currentIndex > 0; currentIndex -= 1) {
      const randomIndex = Math.floor(Math.random() * (currentIndex + 1));
      const tempVal = this.cards[currentIndex];
      this.cards[currentIndex] = this.cards[randomIndex];
      this.cards[randomIndex] = tempVal;
    }
  }

  getTopCard() {
    return this.cards.pop();
  }
}

// Player object.
class Player {
  constructor(name) {
    this.name = name;
    this.hand = []; // player hand containing cards during round.
    this.score = 0;
    this.wins = 0;
  }

  countScore() {
    let aceCount = 0;
    let sum = 0;

    this.hand.forEach((card) => {
      if (card.value === 'J' || card.value === 'Q' || card.value === 'K') {
        sum += 10;
      } else if (card.value === 'A') {
        sum += 11;
        aceCount += 1;
      } else {
        sum += parseInt(card.value, 10);
      }
    });

    while (sum > 21 && aceCount > 0) {
      sum -= 10;
      aceCount -= 1;
    }
    this.score = sum;
  }

  won() {
    this.wins += 1;
  }

  deal(card) {
    this.hand.push(card);
    this.countScore();
  }

  handFull() {
    return this.hand.length === 5;
  }

  isBusted() {
    return this.score > 21;
  }

  isBlackjack() {
    return this.score === 21;
  }
}

// Game object.
class Game {
  constructor() {
    this.hitBtn     = document.getElementById('btn-hit');
    this.standBtn   = document.getElementById('btn-stand');
    this.restartBtn = document.getElementById('btn-restart');
    this.textResult = document.getElementById('text-game-result');
    this.textPlayerWins = document.getElementById('player-score');
    this.textDealerWins = document.getElementById('dealer-score');
    this.player = new Player('player');
    this.dealer = new Player('dealer');
    this.players = [this.player, this.dealer];
    this.endGameButtons();
    this.standBtn.addEventListener('click', this.stand.bind(this));
    this.hitBtn.addEventListener('click', this.hit.bind(this));
    this.restartBtn.addEventListener('click', this.initialize.bind(this));
  }

  // Create a new game.
  initialize() {
    // Reset the card slots
    this.slots = document.getElementsByClassName('slot');
    for (let i = 0; i < this.slots.length; i += 1) {
      this.slots[i].className = 'slot';
    }

    // Create the elements in the game.
    this.player.hand = [];
    this.dealer.hand = [];
    this.endGame = false;
    this.standState = false;
    this.gameInProgress = false;
    this.deck = new Deck();
    this.deck.shuffle();
    this.dealStartingHand();
    this.update();
  }

  // Deal the first 4 cards in the game.
  dealStartingHand() {
    for (let i = 0; i < 2; i += 1) {
      this.players.forEach((player) => {
        player.deal(this.deck.getTopCard());
      });
    }
  }

  hit() {
    this.player.deal(this.deck.getTopCard());
    if (this.player.handFull()) {
      this.hitBtn.disabled = true;
    }
    this.update();
  }

  stand() {
    // End game, dealer turn and final check of scores
    this.standState = true;
    this.endGame = true;

    while (this.dealer.score <= 16) {
      this.dealer.deal(this.deck.getTopCard());
    }

    this.update();
  }


  checkGameState() {
    if (this.player.isBlackjack()) {
      if (this.dealer.isBlackjack()) {
        this.text = 'You and the dealer got blackjack! You lose. Dealer wins.';
        this.dealer.won();
      } else {
        this.text = `Congratulations! Blackjack! Dealer got ${this.dealer.score}. You win.`;
        this.player.won();
      }
    } else if (this.dealer.isBlackjack()) {
      this.text = 'Dealer got blackjack! You lose. Dealer wins.';
      this.dealer.won();
    } else if (this.player.isBusted()) {
      this.text = `You got ${this.player.score}. Busted! You lose.`;
      this.dealer.won();
    } else if (this.dealer.isBusted()) {
      this.text = 'Congratulations! Dealer went over 21. You win!';
      this.player.won();
    } else if (this.standState) {
      if (this.player.score == this.dealer.score) {
        this.text = `You tied with dealer at ${this.player.score}. You lose. Dealer wins`;
        this.dealer.won();
      } else if (this.player.score > this.dealer.score) {
        this.text = `You got ${this.player.score}. Dealer got ${this.dealer.score}. You win!`;
        this.player.won();
      } else if (this.player.score < this.dealer.score) {
        this.text = `You got ${this.player.score}. Dealer got ${this.dealer.score}. You lose!`;
        this.dealer.won();
      }
    } else if (this.player.hand.length === 5) {
      this.text = 'Congratulations! You managed to get 5 cards without busting. You win!';
      this.player.won();
    } else {
      this.text = `Your score is ${this.player.score}. Hit or stand?`;
      return;
    }
    this.endGame = true;
  }

  // Update game state/variables.
  update() {
    this.checkGameState();
    this.render();
  }

  // Render the slots, scores.
  render() {
    this.players.forEach((player) => {
      for (let i = 0; i < player.hand.length; i += 1) {
        const slot = document.getElementById(`${player.name}-card${i + 1}`);
        slot.classList.add(`${player.hand[i].suit}-${player.hand[i].value}`);
      }
    });

    if (this.endGame) {
      document.getElementById('dealer-card2').classList.remove('face-down');
      this.endGameButtons();
    } else {
      document.getElementById('dealer-card2').classList.add('face-down');
      this.ongoingGameButtons();
    }

    this.textResult.textContent = this.text;

    if (this.endGame) {
      this.textPlayerWins.textContent = this.player.wins;
      this.textDealerWins.textContent = this.dealer.wins;
    }
  }

  endGameButtons() {
    this.hitBtn.disabled = true;
    this.standBtn.disabled = true;
    this.restartBtn.disabled = false;
  }

  ongoingGameButtons() {
    this.hitBtn.disabled = false;
    this.standBtn.disabled = false;
    this.restartBtn.disabled = true;
  }
}

// Initialize game on load.
function initializeGame() {
  window.game = new Game();
}

window.onload = initializeGame();
